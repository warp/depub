;;; depub.el --- tool for restoring lost information from pdf conversions -*- lexical-binding: t -*-

;; Copyright 2019 Walter Lewis
;; Author: Walter Lewis
;; Keywords: org, pdf, epub
;; Version: 0.1
;; Package-Requires: (s hydra avy)

;;; Code:

(require 's)
(require 'hydra)
(require 'map)

;; -----------------------------------------------------------------------------
;; common
;; -----------------------------------------------------------------------------

(defconst depub-posframe-buffer "*leaves popup*")

(defun depub-background () (face-foreground 'default))
(defun depub-foreground () (face-background 'default))
(defun depub-face () `(:background ,(depub-background) :foreground ,(depub-foreground)))

(defun depub-walk (regexp step-fn &rest state)
  (let ((region-p (use-region-p)))
    (when region-p
      (goto-char (region-beginning)))
    (while (re-search-forward regexp (when region-p (region-end)) t)
      (let ((str (match-string-no-properties 0)))
        (condition-case err
            (setq state (apply step-fn state))
          (error (progn (replace-match str)
                        (error "Aborting walk: %s" err))))))))

(defun depub-step-replace (replacement &optional lines)
  "Return nullary step fn replacing matches with REPLACEMENT.
With non-nil LINES, move up LINES lines after replacing."
  (lambda ()
    (depub-prompt-replace
     (depub-y-or-n-replace-map replacement))
    (when lines
      (previous-line lines))
    nil))

(defun depub-prompt-replace (rmap)
  (let ((choice nil)
        (start (match-beginning 0))
        (end (match-end 0)))
    (add-text-properties start end '(font-lock-face isearch))
    (save-match-data
      (depub-reveal)
      (recenter)
      (depub-page-sync)
      (depub-show-prompt
       (depub-replace-map-prompt rmap) end)
      (setq choice (map-elt rmap (read-key)))
      (depub-hide-prompt))
    (if (null choice)
        (error "Not a valid replacement key")
      (let ((replacement (car choice))
            (altered (cdr choice)))
        (replace-match replacement)
        (when altered
          (pulse-momentary-highlight-region
           start
           (+ start (length replacement))
           'isearch))
        altered))))

(defun depub-replace-map-prompt (rmap)
  (mapconcat
   (lambda (opt)
     (let ((data (cdr opt)))
       (apply
        #'propertize
        (format "[%s] %s" (string (car opt)) (car data))
        (when (cdr data) '(face bold)))))
   rmap "\n"))

(defun depub-y-or-n-replace-map (replacement)
  `((?y ,(match-substitute-replacement replacement) t)
    (?n ,(match-string-no-properties 0))))

(defun depub-numeric-replace-map (replacements)
  (save-match-data
    (let ((len (length replacements)))
      (cl-mapcar #'list
                 (number-sequence ?1 (+ ?1 len))
                 replacements
                 (make-list len t)))))

(defun depub-hyphen-replacements (pre hyphen post)
  `(,(concat pre post)
    ,(concat pre hyphen post)
    ,(concat pre hyphen " " post)))

(defun depub-show-prompt (string pos)
  (if (bound-and-true-p posframe-show)
      (posframe-show
       depub-posframe-buffer
       :string string
       :position pos
       :background-color (depub-background)
       :foreground-color (depub-foreground))
    (message string)))

(defun depub-hide-prompt ()
  (when (bound-and-true-p posframe-hide)
    (posframe-hide depub-posframe-buffer)))

(defun depub-delete-prompt ()
  (when (bound-and-true-p posframe-delete)
    (posframe-delete depub-posframe-buffer)))

(defun depub-reveal ()
  "Reveals the area around point when in `org-mode'."
  (when (derived-mode-p 'org-mode)
    (save-match-data
      (org-reveal))))

(defun depub-page-sync ()
  "Calls `pdf-page-sync' when in `pdf-page-mode'."
  (when (leaves-live-p)
    (leaves--update-leaf))
  (when (leaves-sync-live-p)
    (leaves-sync-pos)))

(defun depub-normalize-point ()
  "Ensures the point is left at the right spot, accounting for
`evil-mode'."
  (when (bound-and-true-p evil-mode)
    (unless (evil-visual-state-p)
      (backward-char))))

;; --------------------------------------------------------------------------------
;; broken lines
;; --------------------------------------------------------------------------------

;; todo: regexp broken when matching up to the end of a block element, e.g.:
;; 
;; > ending of a block-
;; > quote
;;
;; results in:
;;
;; > ending of a block-quote
;; >
;; 

(defcustom depub-word-regexp
  "[[:alpha:]]+"
  "Matches one or more word chars.")

(defcustom depub-hyphen-regexp
  "-+\\|[–—]"
  "Matches a hyphen.")

(defcustom depub-line-prefix-regexp
  "[>| ][[:space:]]*"
  "Matches a line prefix, before any words.")

(defcustom depub-punct-regexp
  "[^[:space:][:alpha:]]*"
  "Matches any non-word, non-whitespace chars following a word.")

(defun depub--re-group (str &optional num)
  (concat
   "\\(?" (when num (number-to-string num))
   ":"
   str
   "\\)"))

(defvar depub-lnbreak-regexp
  (concat
   (depub--re-group depub-word-regexp 1)
   (depub--re-group depub-hyphen-regexp 2)
   (depub--re-group
    (concat
     "[[:space:]]"
     (depub--re-group depub-line-prefix-regexp) "?")
    3)
   (depub--re-group depub-word-regexp 4)
   (depub--re-group depub-punct-regexp 5)
   (depub--re-group depub-word-regexp 6) "?"
   "[[:space:]]?"))

(defun depub-lnbreak-query ()
  (interactive)
  (depub-walk depub-lnbreak-regexp #'depub-step-lnbreak))

(defun depub-step-lnbreak (&rest st)
  (depub-prompt-replace
   (depub-numeric-replace-map
    (depub-hyphen-replacements
     (match-string-no-properties 1)
     (match-string-no-properties 2)
     (concat (match-string-no-properties 4)
             (match-string-no-properties 5)
             (match-string-no-properties 6)
             (match-string-no-properties 3))))))

;; -----------------------------------------------------------------------------
;; footnote
;; -----------------------------------------------------------------------------

(defcustom depub-fn-ref-regexp
  '("\\([^0-9-–]\\)[[:space:]]?\\([0-9]+\\)\\([][:space:]):—]\\)" . "\\1[^\\2]\\3")
  "Dotted pair whose car is a regular expression matching a
footnote reference, and whose cdr is the replacement text.")

(defcustom depub-fn-text-regexp
  '("^\n* *\\([0-9]+\\)\.?\\([[:space:]]+\\)" . "\n[^\\1]: ")
  "Dotted pair whose car is a regular expression matching the
beginning of a footnote text block, and whose cdr is the
replacement text.")

(defun depub-fn-ref ()
  (interactive)
  (depub-walk-numeric (car depub-fn-ref-regexp) (cdr depub-fn-ref-regexp) 2))

(defun depub-fn-text ()
  (interactive)
  (depub-walk-numeric (car depub-fn-text-regexp) (cdr depub-fn-text-regexp) 1))

(defun depub-step-numeric (subexp replacement init)
  (lambda (iter)
    (save-match-data
      (setq num (string-to-number (match-string-no-properties subexp))))
    (when (and (= num iter)
               (depub-prompt-replace
                (depub-y-or-n-replace-map replacement)))
      (setq iter (1+ iter)))
    (list iter)))

(defun depub-walk-numeric (regexp replacement subexp)
  (let ((init (read-number "Beginning with footnote: " 1)))
    (depub-walk regexp
      (depub-step-numeric subexp replacement init)
      init)))

;; -----------------------------------------------------------------------------
;; paragraph
;; -----------------------------------------------------------------------------

(defcustom depub-para-indent-regexp
  '("\\(\\(?:^[^ 0-9]\\|^[0-9]+[^.]\\).*\n\\)\\(?: \\{2,\\}\\)\\([^[:space:]].*\n[^[:space:]]\\)"
    . "\\1\n\\2")
  "Regular expression and replacement text for replacing a
paragraph indent (such as generated by `pdftotext -layout') with
a line break.")

(defcustom depub-para-hanging-regexp
  '("\\(^[^[:space:]].*\n\\)\\(\\(?: \\{2,\\}[^[:space:]].*\n\\)*\\)"
    . "\n\\1\\2")
  "Regular expression and replacement text for replacing a
hanging indent (such as generated by `pdftotext -layout') with a
line break.")

(defun depub-break-indent ()
  (interactive)
  (depub-walk (car depub-para-indent-regexp)
    (depub-step-replace (cdr depub-para-indent-regexp) 1)))

(defun depub-break-hanging ()
  (interactive)
  (depub-walk (car depub-para-hanging-regexp)
    (depub-step-replace (cdr depub-para-hanging-regexp) 1)))

(defcustom depub-para-break-regexp
  "\\(?1:[.?\"]\n\\)\\(?2:[[:upper:][:digit:]]\\)\\|\\(?1:[^\n]\n\\)\\(?2:[[:upper:]]\\{2,\\}\\)"
  "Regular expression matching paragraph break candidates. Must
  be grouped into two subexpressions: end-of-paragraph subexp
  followed by beginning-of-paragraph subexp.")

;; -----------------------------------------------------------------------------
;; emphasis
;; -----------------------------------------------------------------------------

(defconst depub-emph-border-regexp
  ;; (mapconcat
  ;;  (lambda (emph)
  ;;    (let ((marker (car emph)))
  ;;      (if (string= marker "*") "\\*" marker)))
  ;;  org-emphasis-alist "")
  nil
  "String of recognized emphasis characters.")

(defconst depub-emph-outer-regexp
  (format "[:word:]%s" depub-emph-border-regexp)
  "String of chars NOT considered outside emphasis bounds.")

(defconst depub-emph-pre
  (format "[^%s][%s]?"
   depub-emph-outer-regexp
   depub-emph-border-regexp))

(defconst depub-emph-post
  (format "[%s]?[^%s]"
   depub-emph-border-regexp
   depub-emph-outer-regexp))

(defvar depub-default-emph nil
  "Default emphasis marker to use with `depub-emph-toggle'.")

(defhydra hydra-depub-emph ()
  "
^Emphasis^          ^Jump^              ^Mark
^-^-----------------^-^-----------------^-^-----------------
_E_: Emphasize      _f_: Forward        _w_: Word
_e_: Repeat/toggle  _b_: Backward       ^ ^
"
  ("w" depub-emph-mark-word nil)
  ("f" depub-emph-search nil)
  ("b"
   (let ((current-prefix-arg t))
     (call-interactively #'depub-emph-search))
   nil)
  ("E" depub-emph-select nil)
  ("e" depub-emph-toggle nil)
  )

(global-set-key (kbd "C-c e") 'hydra-depub-emph/body)

(defun depub-emph-mark-word (&optional auto)
  "Mark the word at point, including emphasis markers.

AUTO should be non-nil if the mark will be used again within the
same command. This delays the call to `depub-normalize-point'."
  (interactive)
  (forward-char)
  (re-search-backward depub-emph-pre nil 'limit)
  (forward-char)
  (set-mark (point))
  (re-search-forward depub-emph-post nil 'limit)
  (backward-char)
  (activate-mark)
  (unless auto (depub-normalize-point))) 

(defun depub-emph-toggle ()
  "Toggle default emphasis of the marked text or word under
cursor."
  (interactive)
  (unless (use-region-p)
    (depub-emph-mark-word t))
  (let ((deactivate-mark nil))
    (org-emphasize
     (if (depub-emph-get (region-beginning) (region-end))
         ?\s
       depub-default-emph)))
  (depub-emph-remember)
  (depub-normalize-point))

(defun depub-emph-select ()
  "Choose emphasis of the marked text or word under cursor and
set as the default for `depub-emph-toggle'."
  (interactive)
  (unless (use-region-p)
    (depub-emph-mark-word t))
  (let ((deactivate-mark nil))
    (call-interactively #'org-emphasize))
  (depub-emph-remember)
  (depub-normalize-point))

(defun depub-emph-search (&optional backward)
  "Find the next occurrence of marked text or word under cursor,
possibly emphasized.

Search backward if prefix arg is non-nil."
  (interactive "P")
  (unless (use-region-p)
    (depub-emph-mark-word t))
  (let* ((case-fold-search nil)
         (phrase-beg (region-beginning))
         (phrase-end (region-end))
         (step (if backward -1 1))
         (pad (if (depub-emph-get phrase-beg phrase-end) 1 0)))
    (when (re-search-forward
           (format
            "[^%3$s]\\([%2$s]?\\)%1$s\\1[^%3$s]"
            (s-join
             "[[:space:]]?"
             (s-split-words 
              (buffer-substring-no-properties
               (+ phrase-beg pad)
               (- phrase-end pad))))
            depub-emph-border-regexp
            depub-emph-outer-regexp)
           nil t step)
      (set-mark (if backward (- (match-end 0) 1) (+ (match-beginning 0) 1)))
      (backward-char step)
      (depub-reveal)
      (depub-page-sync)
      (depub-normalize-point))))

(defun depub-emph-get (beg end)
  "Non-nil if an emphasis marker from `org-emphasis-alist'
matches both the char at BEG and the char just after END."
  (let ((pre (char-after beg))
        (post (char-before end)))
    (if (and (eq pre post)
             (assoc (string pre) org-emphasis-alist))
        pre
      nil)))

(defun depub-emph-remember ()
  (setq depub-default-emph
        (or (depub-emph-get (region-beginning) (region-end))
            depub-default-emph)))

;; --------------------------------------------------------------------------------
;; figures
;; --------------------------------------------------------------------------------

(defcustom depub-fig-regexp
  "fig\\.[[:space:]]\\([0-9]+\\.[0-9]+\\)"
  "Regular expression matching reference to a figure.")

(defcustom depub-fig-range-regexp
  "figs\\.[[:space:]]\\([0-9]+\\)\\.\\([0-9]+\\)[-–]\\([0-9]+\\)"
  "Regular expression matching figure ranges.")

(defun depub-fig-link-all ()
  (interactive)
  (save-excursion (depub-fig-link))
  (depub-fig-link-range))

(defun depub-fig-link ()
  (interactive)
  (depub-walk depub-fig-regexp
    (depub-step-replace "[[fig. \\1]]")))

(defun depub-fig-link-range ()
  (interactive)
  (depub-walk depub-fig-range-regexp
    (lambda ()
      (depub-prompt-replace
       (depub-y-or-n-replace-map
        (depub-fig-expand-range
         ""
         (match-string 1)
         (string-to-number (match-string 2))
         (string-to-number (match-string 3))))))))

(defun depub-fig-expand-range (str base iter bound)
  (let ((expanded (format "%s[[fig. %s.%s]]" str base iter)))
    (if (>= iter bound)
	expanded
      (depub-fig-expand-range (format "%s, " expanded) base (1+ iter) bound))))

;; tests:
;;
;; fig. 1.13 -> [[fig. 1.13]]
;;
;; figs. 1.13-15 -> [[fig. 1.13]], [[fig. 1.14]], [[fig. 1.15]]

(provide 'depub)

;;; depub.el ends here
